import uproot
import numpy as np
import torch
import jax.numpy as jnp
import jraph
import jax.random as jrandom
import jax.tree_util as tree
import jax
import flax 
import haiku as hk
from typing import Any, Callable, Dict, List, Optional, Tuple
import optax
import os
import itertools
import matplotlib.pyplot as plt 
from jraph._src import models



from jax.lib import xla_bridge
print('Using platform: ', xla_bridge.get_backend().platform)

#os.environ["XLA_PYTHON_CLIENT_PREALLOCATE"]="false"
#os.environ["XLA_PYTHON_CLIENT_MEM_FRACTION"]=".XX"
#os.environ["XLA_PYTHON_CLIENT_ALLOCATOR"]="platform"


f = uproot.open("tksim_bg2.0_file87.root")
trees = f['tksim']
cell_variables = ['yDigY', 'yDigX', 'yDigPdgId', 'yDigZ']
events = trees['yDigZ']

nevents_tot = int(trees.num_entries)
X_tot = np.asarray(trees['yDigX'])/1000
Y_tot = np.asarray(trees['yDigY'])/1000
Z_tot = np.asarray(trees['yDigZ'])/1000
ID_tot = np.asarray(trees['yDigPdgId'])
arrays = np.asarray((X_tot, Y_tot, Z_tot, ID_tot))

start_training = 0
end_training= 100
batch_size = 10
arrays = np.asarray((X_tot[start_training:end_training], Y_tot[start_training:end_training], Z_tot[start_training:end_training], ID_tot[start_training:end_training]))
print('Events for training: ', arrays.shape)

start_test = end_training + 1
end_test= start_test + 100

arrays_test = np.asarray((X_tot[start_test:end_test], Y_tot[start_test:end_test], Z_tot[start_test:end_test], ID_tot[start_test:end_test]))
print('Events for testing: ', arrays_test.shape)

## functions for graph creation ##


## functions for graph creation ##

#function that compute the node number (hit number) for each graph (event) in the batch
def get_node_number(arrays, batch_size):
    node_numbers = []
    x = arrays[0]
    
    for i in range(batch_size):
        
        xx = x[i]
        node_numbers.append([xx.shape[0]])
    #max_value = np.max(node_numbers)
    max_value = 384
    
    return node_numbers, max_value

#function that returns the n_nodes and n_edges vectors required for defining the GraphTuple
#n_nodes vector is like ([a],[b],[c]) with a,b,c nodes numbers for each graph in the batch
#n_edges vector is like ([a**2], [b**2], [c**2]) where each element is the number of edges for the graph (fully connected)

def get_nodes_edges_per_event(arrays, batch_size):
    data_array = get_node_number(arrays, batch_size)[0]

    hits_per_event = data_array
    edges_per_event = [[nhits[0]**2] for nhits in data_array]
    return hits_per_event, edges_per_event


#function that creates for each graoh in the batch the vectors defining senders and receivers
#explicitly batched graph is used so all the vectors are padded to the dimension of the bigger one 

def create_senders_receivers(arrays, batch_size):
    #nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    nhits = get_nodes_edges_per_event(arrays, batch_size)[0]
    senders = []
    receivers = []
    for nhit in nhits:
        n = nhit[0]
        s = torch.arange(n).repeat(n).tolist()
        r = torch.repeat_interleave(torch.arange(n), n).tolist()
        
        senders.append(s)
        receivers.append(r)

    padded_senders = []
    padded_receivers = []
    max_value = 384

    for sender in senders:
        pad_s = sender + [-1] * (max_value**2 - len(sender))
        padded_senders.append(pad_s)

    for receiver in receivers:
        pad_r = receiver + [-1] * (max_value**2 - len(receiver))
        padded_receivers.append(pad_r)

    return padded_senders, padded_receivers


#function that decorates the target nodes with only the particle id (muon == -13.)
def decorate_nodes_truth(arrays, batch_size):
    nodes = []
    max_value = 384
    
    for i in range (0, batch_size):        
        id = np.asarray(arrays[3][i])
        padded_id =  jnp.pad(id, (0, max_value - len(id)), mode='constant')
        node_features = jnp.stack((padded_id), axis=-1)
        nodes.append(node_features)

    return nodes

#function that decorates the input nodes with (y,z) coordinates
def decorate_nodes(arrays,batch_size):
    nodes = []
    #max_value = get_node_number(arrays, batch_size)[1]   
    max_value = 384 
    for i in range (0, batch_size):
        y = np.asarray(arrays[1][i])
        z = np.asarray(arrays[2][i])
        padded_y =  jnp.pad(y, (0, max_value - len(y)), mode='constant')
        padded_z =  jnp.pad(z, (0, max_value - len(z)), mode='constant')
      
        node_features = jnp.stack((padded_y,padded_z), axis=-1)
        nodes.append(node_features)

    return nodes

def GetGraphs(arrays: np.ndarray, batch_size : int) -> jraph.GraphsTuple:
    graph = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    graph_truth = jraph.GraphsTuple(
        n_node=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[0]), 
        n_edge=jnp.array(get_nodes_edges_per_event(arrays, batch_size)[1]), 
        nodes=jnp.array(decorate_nodes_truth(arrays, batch_size)), 
        edges=None, 
        globals=None,  
        senders=jnp.array(create_senders_receivers(arrays, batch_size)[0]), 
        receivers=jnp.array(create_senders_receivers(arrays, batch_size)[1]))
    
    return (graph, graph_truth)


## network definition ##

from jraph._src import utils

def add_self_edges_fn(receivers: jnp.ndarray, senders: jnp.ndarray,
                      total_num_nodes: int) -> Tuple[jnp.ndarray, jnp.ndarray]:
  """Adds self edges. Assumes self edges are not in the graph yet."""
  receivers = jnp.concatenate((receivers, jnp.arange(total_num_nodes)), axis=0)
  senders = jnp.concatenate((senders, jnp.arange(total_num_nodes)), axis=0)
  return receivers, senders
'''
import jax.numpy.linalg as LA

def gat_definition(graph: jraph.GraphsTuple) -> jraph.GraphsTuple:

    def _attention_query_fn(node_features):
        return hk.nets.MLP([64,32])(node_features)

    def _attention_logit_fn(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        #return hk.nets.MLP([100,50,25,10,1])(jax.nn.leaky_relu(hk.nets.MLP([1000,800,600,400,200,100])(feat)))
        return hk.nets.MLP([64, 32,1])(jax.nn.leaky_relu(hk.nets.MLP([128,64])(feat)))
    

    gn = models.GAT(_attention_query_fn, _attention_logit_fn, node_update_fn=hk.nets.MLP([128,64,8,1]))
    graph = gn(graph)

    return graph
'''
#################
# GAT implementation adapted from https://github.com/deepmind/jraph/blob/master/jraph/_src/models.py#L442.
def GAT(attention_query_fn: Callable,
        attention_logit_fn: Callable,
        node_update_fn: Optional[Callable] = None,
        add_self_edges: bool = True) -> Callable:
  
  # pylint: disable=g-long-lambda
  if node_update_fn is None:
    # By default, apply the leaky relu and then concatenate the heads on the
    # feature axis.
    node_update_fn = lambda x: jnp.reshape(
        jax.nn.leaky_relu(x), (x.shape[0], -1))

  def _ApplyGAT(graph: jraph.GraphsTuple) -> jraph.GraphsTuple:
    """Applies a Graph Attention layer."""
    nodes, edges, receivers, senders, _, _, _ = graph
    # Equivalent to the sum of n_node, but statically known.
    try:
      sum_n_node = nodes.shape[0]
    except IndexError:
      raise IndexError('GAT requires node features')

    # Pass nodes through the attention query function to transform
    # node features, e.g. with an MLP.
    nodes = attention_query_fn(nodes)

    total_num_nodes = tree.tree_leaves(nodes)[0].shape[0]
    if add_self_edges:
      # We add self edges to the senders and receivers so that each node
      # includes itself in aggregation.
      receivers, senders = add_self_edges_fn(receivers, senders,
                                             total_num_nodes)

    # We compute the softmax logits using a function that takes the
    # embedded sender and receiver attributes.
    sent_attributes = nodes[senders]
    received_attributes = nodes[receivers]
    att_softmax_logits = attention_logit_fn(sent_attributes,
                                            received_attributes, edges)

    # Compute the attention softmax weights on the entire tree.
    att_weights = jraph.segment_softmax(
        att_softmax_logits, segment_ids=receivers, num_segments=sum_n_node)

    # Apply attention weights.
    messages = sent_attributes * att_weights
    # Aggregate messages to nodes.
    nodes = jax.ops.segment_sum(messages, receivers, num_segments=sum_n_node)

    # Apply an update function to the aggregated messages.
    nodes = node_update_fn(nodes)

    return graph._replace(nodes=nodes)

  # pylint: enable=g-long-lambda
  return _ApplyGAT



def gat_definition(graph: jraph.GraphsTuple) -> jraph.GraphsTuple:
  """Defines a GAT network for the karate club node classification task.

  Args:
    graph: GraphsTuple the network processes.

  Returns:
    output graph with updated node values.
  """



  def _attention_query_fn(node_features):
        return hk.nets.MLP([32, 64])(node_features)
  
  def _attention_logit_fn(senders, receivers, edges):
        del edges
        feat = jnp.concatenate((senders, receivers), axis=-1)
        #return hk.nets.MLP([100,50,25,10,1])(jax.nn.leaky_relu(hk.nets.MLP([1000,800,600,400,200,100])(feat)))
        return hk.nets.MLP([128, 64, 32,1])(jax.nn.leaky_relu(hk.nets.MLP([32, 64, 128, 256, 128])(feat)))
    


  gn = GAT(
      attention_query_fn=_attention_query_fn,

      attention_logit_fn=_attention_logit_fn,
      node_update_fn=hk.nets.MLP([128,64, 32]),
      add_self_edges=True)
  graph = gn(graph)

  gn = GAT(
      attention_query_fn=_attention_query_fn,

      attention_logit_fn=_attention_logit_fn,
      node_update_fn=hk.nets.MLP([128,64,8,1]),
      add_self_edges=True)
  graph = gn(graph)
  return graph


#################
print('Graph definition')
graph = GetGraphs(arrays, batch_size)[0]
graph_truth = GetGraphs(arrays,batch_size)[1]

def DataLoader(arrays, batch_size, *, key):
    dataset_size = arrays[0].shape[0]
    #print('n_events: ', dataset_size, ', batch_size: ', batch_size)
        
    (key,) = jrandom.split(key, 1)
    start = 0
    end = batch_size
    while end <= dataset_size:
        
        yield tuple(GetGraphs(arrays[:,start:end], batch_size))
        start = end
        end = start + batch_size

## network inizialization ##
print('Network inizialization') 
network = hk.without_apply_rng(hk.transform(hk.vmap(gat_definition, split_rng=False)))
params = network.init(jax.random.PRNGKey(42), graph)

opt_init, opt_update = optax.adam(1e-5)
opt_state = opt_init(params)

## loss definition ##


loss_list = []
muon_loss_list = []
bkg_loss_list = []

@jax.jit
def prediction_loss(params, input_graph, target_graph):

        output_graph = network.apply(params, input_graph)
        
        #print(input_graph.nodes.shape, output_graph.nodes.shape)
        radius = np.reshape(output_graph.nodes, (output_graph.nodes.shape[0], output_graph.nodes.shape[1]))
        #print(jnp.isnan(radius))
        #radius = jnp.reshape(radius, (radius.shape[0], radius.shape[1]))
        id_mask = target_graph.nodes
        #print(id_mask.shape, radius.shape)
        #print(radius.ravel())

        muon_loss = (radius - 1)**2   
        #print(muon_loss.shape)     
        masked_muon_loss = jnp.where(jnp.absolute(id_mask) == 13.0, muon_loss, 0)

        muon_loss_list.append(masked_muon_loss.sum() / jnp.count_nonzero(masked_muon_loss))
        muon_loss =  masked_muon_loss.sum() / jnp.count_nonzero(masked_muon_loss)

        bkg_loss = (radius)**2
        masked_bkg_hit_loss = jnp.where((jnp.absolute(id_mask) != 13.0) , bkg_loss, 0)
        bkg_loss_list.append(masked_bkg_hit_loss.sum() / jnp.count_nonzero(masked_bkg_hit_loss))
        bkg_loss =  masked_bkg_hit_loss.sum() / jnp.count_nonzero(masked_bkg_hit_loss)
        #print('MUON LOSS: ', muon_loss)
        #print('BKG LOSS: ', bkg_loss)
        #print(jnp.isnan(muon_loss))
        #print(jnp.isnan(bkg_loss))
        
        return muon_loss + bkg_loss


@jax.jit
def update(params, opt_state, gr):
        """Returns updated params and state."""
        updates, opt_state = opt_update(gr, opt_state)
        return optax.apply_updates(params, updates), opt_state

steps = 5000
dataloader = DataLoader(arrays, batch_size,  key = jrandom.PRNGKey(68743))
it = itertools.tee(dataloader, steps) 



print('Training.. ')
for step in range(steps):
    
        epoch_loss = 0
        iter_data = it[step]
        for g in iter_data:
                input_graph = g[0]
                truth_graph = g[1]
                #print('-- in loss ---')
                loss = prediction_loss(params, input_graph, truth_graph)
                #print('--- in grad --- ')
                gr = jax.grad(prediction_loss, argnums=0)(params, input_graph, truth_graph)
                #print(gr)
                epoch_loss = epoch_loss + loss
               
                params, opt_state = update(params, opt_state, gr)

        print('--------------------->  STEP: ', step, ' LOSS: ', epoch_loss)
        loss_list.append(epoch_loss)


plt.figure(figsize=(7, 5), dpi=100)
xrange = np.arange(0,steps,1)
plt.plot(xrange, np.asarray(loss_list), label = 'loss')
#plt.plot(xrange, np.asarray(muon_loss_list), label = 'muon')
#plt.plot(xrange, np.asarray(bkg_loss_list), label = 'bkg')
plt.xlabel('epochs')
plt.ylabel('value')
plt.legend()
plt.savefig('loss.png')

## post processing

print('Post processing ..')
predicted_graph = network.apply(params, graph)
predicted_radius = predicted_graph.nodes
predicted_radius = np.reshape(predicted_radius, (predicted_radius.shape[0], predicted_radius.shape[1]))
r = predicted_radius.flatten()
truth = truth_graph.nodes.flatten().tolist()
mu = []
bkg = []
for i in range(len(truth)):
    radius = r[i]
    t = abs(truth[i])

    if t == 13:
        mu.append(radius)
    else:
        bkg.append(radius)

    
plt.figure(figsize=(7, 5), dpi=100)
print(len(mu), len(bkg), len(r))

plt.hist(bkg, bins=100, range = [0,1.5], label = 'bkg', alpha = 0.5)
plt.hist(mu, bins = 100, range = [0,1.5], label = 'signal', alpha = 0.5)

plt.legend()
plt.savefig('predictions.png')